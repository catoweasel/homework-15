#include <iostream>

void Parity(int N, bool IsEven)
{
    for (int i = IsEven; i <= N; i += 2)
    {
        std::cout << i << " ";
    }
}

int main()
{
    const int N = 13;
    bool IsEven = N % 2 == 1;

    for (int i = 0; i <= N; i++)
    {
        if (i % 2 == 0)
        {
            std::cout << i << " ";
        }
    }

    std::cout << "\n";

    Parity(N, IsEven);

    return 0;
}